package com.example.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService {

	@Autowired
	private PostRepository postrepository;

	public Post savePost(Post po) {
		return postrepository.save(po);
	}

	public List<Post> getAllPo() {
		List<Post> li = postrepository.findAll();
		return li;
	}

}
