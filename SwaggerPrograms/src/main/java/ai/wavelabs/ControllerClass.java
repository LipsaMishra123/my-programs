package ai.wavelabs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class ControllerClass<APIResponse> {
	
	
	List<Student> list1 = new ArrayList<Student>();

	@GetMapping(value = "/students")
	public ResponseEntity<List<Student>> getStudent() {
		return ResponseEntity.ok().body(list1);
	}

	

	@PostMapping("students")
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		// list.add(student);
		return ResponseEntity.status(201).body(student);
	}

	
}
