package com.asynchronous;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAsynchronousApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAsynchronousApplication.class, args);
	}

}
