package com.example1;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="department")
public class Department {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer Id;
	@Column
	private String DeptName;
	@Column
	private String DeptDescription;

	public Department(int Id, String deptName, String deptDescription) {
		super();
		this.Id = Id;
		this.DeptName = deptName;
		this.DeptDescription = deptDescription;
	}
	public int getId() {
		return Id;
	}
	public void setDeptId(int Id) {
		this.Id = Id;
	}
	public String getDeptName() {
		return DeptName;
	}
	public void setDeptName(String deptName) {
		DeptName = deptName;
	}
	public String getDeptDescription() {
		return DeptDescription;
	}
	public void setDeptDescription(String deptDescription) {
		DeptDescription = deptDescription;
	}
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "department_category", catalog = "lipsa", joinColumns = { 
			@JoinColumn(name = "Id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "category_Id", 
					nullable = false, updatable = false) })	
	private Set<Category> categories;

	public Set<Category> getCategories() {
		return categories;
	}
	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
	
	
	
	

}
