package com.example1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;




@RestController
public class DepartmentController {
	@Autowired
	private DepartmentService deptservice;

	@GetMapping(value = "department")
	public ResponseEntity<List<Department>> getAllDept() {
		List<Department> li = deptservice.getAllDept();
		return ResponseEntity.status(200).body(li);
	}

	@PostMapping(value = "department")
	public ResponseEntity<Department> addDept(@RequestBody Department Dept) {
		Department p = deptservice.saveDepartment(Dept);
		return ResponseEntity.status(201).body(p);
	}

	
	
}
