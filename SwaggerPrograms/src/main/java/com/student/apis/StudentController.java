
package com.student.apis;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

//import com.google.common.net.MediaType;
@Service
public class StudentController {
	@Autowired
	StudentService studentService;

	List<Student> list = new ArrayList<>();

	@GetMapping(value = "/studentsjpa", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Student>> getStudents(@RequestParam(name = "age", required = false) Integer age) {
		List<Student> students = studentService.getAllStudents();
		return ResponseEntity.status(200).body(students);
	}

	@PostMapping(value = "/studentsjpa", consumes = { MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		Student s = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(s);
	}

}
