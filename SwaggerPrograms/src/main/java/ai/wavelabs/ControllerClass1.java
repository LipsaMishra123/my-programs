package ai.wavelabs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@SpringBootApplication

public class ControllerClass1 {

	List<Product> list1 = new ArrayList<Product>();

	@GetMapping(value = "/products")
	public ResponseEntity<List<Product>> getProduct() {
		return ResponseEntity.ok().body(list1);

	}

	@PostMapping("products")
	public ResponseEntity<Product> saveProduct(@RequestBody Product product) {
		return ResponseEntity.status(201).body(product);
	}

}
