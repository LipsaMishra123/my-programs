package com.example1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class EmployeeCacheController {
	
	@Autowired
	public EmployeeCacheService empservice;
	
	@GetMapping(value="employeecache")
	public ResponseEntity<List<EmployeeCache>> getEmployeeCache() {

		List<EmployeeCache> employeee = empservice.getAllEmployeeCache();
		return ResponseEntity.status(200).body(employeee);
	}
	@PostMapping(value="employeecache")
	public ResponseEntity<EmployeeCache> updateEmployeeCache(@RequestBody EmployeeCache cacheemp)
	{
		return ResponseEntity.status(201).body(empservice.saveEmployeeCache(cacheemp));
	}
	

}
