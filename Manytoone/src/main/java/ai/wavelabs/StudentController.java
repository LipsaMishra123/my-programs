
package ai.wavelabs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import com.google.common.net.MediaType;

@RestController
public class StudentController {
	@Autowired
	StudentService studentService;

	List<Student> list = new ArrayList<>();

	@GetMapping(value = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Student>> getStudents(@RequestParam(name = "age", required = false) Integer age) {
		List<Student> students = studentService.getAllStudents();
		return ResponseEntity.status(200).body(students);
	}

	@PostMapping(value = "/students", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		Student s = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(s);
	}

	@PutMapping("studentsjpa/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable("id") Integer id, @RequestBody Student student) {
		studentService.updateStudent(id, student);
		return ResponseEntity.status(200).body(student);
	}

	@DeleteMapping("students/{id}")
	public void deleteStudent(@PathVariable("id") Integer id) {
		//studentService.deleteStudent(id);
// return ResponseEntity.status(201);

	}

}
