package com.example.jpa;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostController {

	@Autowired
	private PostService postservice;

	@GetMapping(value = "posts")
	public ResponseEntity<List<Post>> getAllPo() {
		List<Post> li = postservice.getAllPo();
		return ResponseEntity.status(200).body(li);
	}

	@PostMapping(value = "posts")
	public ResponseEntity<Post> addPo(@RequestBody Post po) {
		Post p = postservice.savePost(po);
		return ResponseEntity.status(201).body(p);
	}

}