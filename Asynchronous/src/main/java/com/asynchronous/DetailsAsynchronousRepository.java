package com.asynchronous;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailsAsynchronousRepository extends JpaRepository<DetailsAsynchronous,Integer> {

}
