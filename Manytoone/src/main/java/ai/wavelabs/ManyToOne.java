package ai.wavelabs;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ManyToOne {
   public static void main( String[ ] args ) {
   
   EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "Eclipselink_JPA" );
   EntityManager entitymanager = emfactory.createEntityManager( );
   entitymanager.getTransaction( ).begin( );

   //Create Department Entity
   Department department = new Department();
   department.setName("Development");
   
   //Store Department
   entitymanager.persist(department);

   //Create Employee1 Entity
   Employee employee1 = new Employee();
   employee1.setEname("Lipsa");
   employee1.setSalary(65000.0);
   employee1.setDeg("Full Stack Developer");
   employee1.setDepartment(department);

   //Create Employee2 Entity
   Employee employee2 = new Employee();
   employee2.setEname("Bhavani G C");
   employee2.setSalary(45000.0);
   employee2.setDeg("BackEnd Developer");
   employee2.setDepartment(department);

   //Create Employee3 Entity
   Employee employee3 = new Employee();
   employee3.setEname("Machaa");
   employee3.setSalary(50000.0);
   employee3.setDeg("Technical Writer");
   employee3.setDepartment(department);
   
 //Create Employee4 Entity
   Employee employee4 = new Employee();
   employee4.setEname("Puli Sharma");
   employee4.setSalary(25000.0);
   employee4.setDeg("Sweeper");
   employee4.setDepartment(department);
   
 //Create Employee5 Entity
   Employee employee5 = new Employee();
   employee5.setEname("Vivek Ravi");
   employee5.setSalary(15000.0);
   employee5.setDeg("Security");
   employee5.setDepartment(department);
   
   

   //Store Employees
   entitymanager.persist(employee1);
   entitymanager.persist(employee2);
   entitymanager.persist(employee3);

   entitymanager.getTransaction().commit();
   entitymanager.close();
   emfactory.close();
   }
}