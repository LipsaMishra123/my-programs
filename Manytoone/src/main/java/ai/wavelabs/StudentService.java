package ai.wavelabs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.student.apis.Student;

@Service
public class StudentService 
{
	@Autowired
	private StudentRepository studentRepository;

	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

	public List<Student> getAllStudents() 
	{
		return studentRepository.findAll();
	}
	public Student updateStudent(Integer id, Student student) {
		student.setId(id);
		return studentRepository.save(student);
		}


}
