package com.transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentTransactionController {
	
	@Autowired
	public StudentTransactionService service;
	@GetMapping(value = "studenttransaction")
	public ResponseEntity<List<StudentTransaction>> getStudentTransaction() {

		List<StudentTransaction> studenttt = service.getStudentTransaction();
		return ResponseEntity.status(200).body(studenttt);
	}
	@PostMapping(value="studenttransaction")
	public ResponseEntity<StudentTransaction> updateStudentTransaction(@RequestBody StudentTransaction studenttransaction)
	{
		return ResponseEntity.status(201).body(service.addStudentTransaction(studenttransaction));
	}
	@PutMapping(value="studenttransaction/{studentttNo}")
	public ResponseEntity<StudentTransaction> updateBalance(@RequestBody StudentTransaction transaction,@PathVariable("studentttNo") int studentttNo)
	{
		
		
		return ResponseEntity.status(200).body(service.updateStudentTransaction(transaction, studentttNo));
	}

	

}
