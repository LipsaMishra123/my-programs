package com.transaction;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentTransactionRepository extends JpaRepository<StudentTransaction,Integer>{

}
