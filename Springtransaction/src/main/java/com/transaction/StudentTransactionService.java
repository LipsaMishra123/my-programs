package com.transaction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentTransactionService {
	
	@Autowired
	public StudentTransactionRepository repository;
	
	public StudentTransaction addStudentTransaction(StudentTransaction transaction) {

		return repository.save(transaction);
	}
     @Transactional(rollbackFor  = Exception.class)
	public StudentTransaction updateStudentTransaction(StudentTransaction transaction, int sid) 
     {
    	 int a=10;
    	 transaction.setSid(sid);
	     int b=a/10;
		return repository.saveAndFlush(transaction);

}

	public List<StudentTransaction> getStudentTransaction() 
	{
		List<StudentTransaction>  studenttt=repository.findAll();
		return studenttt;
	}

	

}
