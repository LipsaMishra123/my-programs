package com.asynchronous;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class DetailsAsynchronousService {
	
	@Autowired
	public DetailsAsynchronousRepository repository;
	
	@Async
	public DetailsAsynchronous saveDetailsAsynchronous(DetailsAsynchronous detailsasync)
	{
		DetailsAsynchronous details = repository.save(detailsasync);
		try {
			Thread.sleep(20);
		} catch (InterruptedException o) {
			o.printStackTrace();
		}

		return details;
	}
	@Cacheable(cacheNames = "stroredetails")
	public List<DetailsAsynchronous> getAllDetailsAsynchronous() {
		
		List<DetailsAsynchronous> list=getAllDetailsAsynchronous();
		return list;
	}
	@CachePut(cacheNames = "storedetails", key = "#id")
	@Async
	public DetailsAsynchronous updateDetailsAsynchronous(DetailsAsynchronous store) {
		return repository.saveAndFlush(store);
	}
	

}
